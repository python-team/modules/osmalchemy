Source: osmalchemy
Maintainer: Dominik George <nik@naturalnet.de>
Uploaders:
 Debian Python Team <team+python@tracker.debian.org>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 python3-all,
 python3-dateutil,
 python3-flask (>= 0.10),
 python3-flask-sqlalchemy,
 python3-overpass,
 python3-psycopg2,
 python3-setuptools,
 python3-sqlalchemy (>= 1.0.0),
 python3-testing.mysqld,
 python3-testing.postgresql,
Standards-Version: 3.9.8
Testsuite: autopkgtest-pkg-python
Homepage: https://github.com/Natureshadow/OSMAlchemy
Vcs-Git: https://salsa.debian.org/python-team/packages/osmalchemy.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/osmalchemy

Package: python3-osmalchemy
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python3-flask-sqlalchemy,
Description: OpenStreetMap to SQLAlchemy bridge
 OSMAlchemy is a bridge between SQLAlchemy and the OpenStreetMap API.
 .
 OSMAlchemy's goal is to provide completely transparent integration of
 the real-world OpenStreetMap data within projects using SQLAlchemy. It
 provides two things:
 .
 1. Model declaratives resembling the structure of the main OpenStreetMap
    database, with some limitations, usable wherever SQLAlchemy is used,
    and
 2. Transparent proxying and data-fetching from OpenStreetMap data.
 .
 The idea is that the model can be queried using SQLAlchemy, and
 OSMAlchemy will either satisfy the query from the database directly or
 fetch data from OpenStreetMap.
 .
 OSMAlchemy can work together with Flask-SQLAlchemy for easy use in Flask
 applications, and it can also be used with pure SQLAlchemy.
